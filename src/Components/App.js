import React, { Component } from 'react';
import Familias from "./Familias";
import '../App.css';

class App extends Component {
	constructor() {
		super();
	}

	render() {
		return (
			<div className="App">
				<h1>Guía interactiva - MCRSafety</h1>
				<div class="familias">
					<Familias id="manual" value="manual" label="Protección Manual" />
					<Familias id="visual" value="visual" label="Protección Visual" />
					<Familias id="coporal" value="corporal" label="Protección Corporal" />
				</div>
			</div>

		)
	}

}


export default App;
